init:
	docker exec -it backend-php-fpm bash -c "php init.php"


migrate:
	docker exec -it backend-php-fpm bash -c "php yii.php migrate"