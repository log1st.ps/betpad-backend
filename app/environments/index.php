<?php

$base = [
    'setWritable' => [
        'common/runtime',
        'app/runtime',
        'app/web/assets',
    ],
    'setExecutable' => [
        'yii.php',
        'app/controllers/DevController.php'
    ],
    'setCookieValidationKey' => [
        'common/config/main-local.php'
    ]
];

return [
    'Development' => array_merge($base, [
        'path' => 'dev',
    ]),
    'Production' => array_merge($base, [
        'path' => 'prod',
    ]),
];