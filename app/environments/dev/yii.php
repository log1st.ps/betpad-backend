<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/common/config/bootstrap-local.php';

require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/common/config/bootstrap.php';
require __DIR__ . '/console/config/bootstrap-local.php';
require __DIR__ . '/console/config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/common/config/main.php',
    require __DIR__ . '/common/config/main-local.php',
    require __DIR__ . '/console/config/main.php',
    require __DIR__ . '/console/config/main-local.php'
);

try {
    unset($config['components']['errorHandler']);
    unset($config['components']['request']);
}   catch(\Exception $e) {}

$exitCode = (new \yii\console\Application($config))->run();
exit($exitCode);