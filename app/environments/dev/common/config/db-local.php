<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 16:13
 */

$base = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . env('DB_HOST') . ';port=' . env('DB_PORT') . ';dbname=',
    'username' => env('DB_USER', 'user'),
    'password' => env('DB_PASSWORD', 'password'),
    'charset' => 'utf8',
];

return [
    'db' => \yii\helpers\ArrayHelper::merge($base, [
        'dsn' => $base['dsn'] . env('DB_NAME', 'dbname'),
    ]),
];