<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 16:33
 */

include(dirname(__FILE__, 3) . '/inc/functions.php');

try {
    $dir = dirname(__FILE__, 3);
    $dotenv = \Dotenv\Dotenv::create($dir);
    $dotenv->load();
}   catch(\Exception $e) {

}

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

defined('YII_CACHE') or define('YII_CACHE', false);
defined('YII_CACHE_DURATION') or define('YII_CACHE_DURATION', 60 * 60);

defined('CONST_DOMAIN') || define('CONST_DOMAIN', env('CONST_DOMAIN'));