<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 16:14
 */

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app',
    'basePath' => dirname(__DIR__),
    'params' => $params,
];

return $config;