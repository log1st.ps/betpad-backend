<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 22.10.2017
 * Time: 9:04
 */

namespace console\components;


use Yii;

class Migration extends \yii\db\Migration
{
    const FK_CASCADE = 'CASCADE';
    const FK_RESTRICT = 'RESTRICT';
    const FK_NO_ACTION = 'NO ACTION';
    const FK_SET_NULL = 'SET NULL';
    const FK_SET_DEFAULT = 'SET DEFAULT';

    public function createTimestamps($table)
    {
        $tbl = Yii::$app->db->schema->getTableSchema($table);
        foreach(['created_at', 'updated_at'] as $col) {
            if (isset($tbl->columns[$col])) {
                $this->dropColumn($table, $col);
            }
        }
        $this->addColumn($table, 'created_at', $this->integer(11)->comment('Created At'));
        $this->addColumn($table, 'updated_at', $this->integer(11)->comment('Updated At'));
    }
}