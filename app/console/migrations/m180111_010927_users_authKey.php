<?php
	
	use console\components\Migration;
	
	
	/**
	 * Class m180111_010927_users_authKey
	 */
	class m180111_010927_users_authKey extends Migration
	{
		/**
		 * @inheritdoc
		 */
		public function safeUp()
		{
			$this->addColumn('users', 'authKey', $this->string(255)->comment('Auth Token'));
		}
		
		/**
		 * @inheritdoc
		 */
		public function safeDown()
		{
			$this->dropColumn('users', 'authKey');
		}
	}
