<?php
	
	
	use console\components\Migration;
	
	class m171102_054305_files extends Migration
	{
		public function safeUp()
		{
			$this->createTable('files', [
				'file_id' => $this->primaryKey()->comment('File ID'),
				'url' => $this->text()->comment('URL'),
				'type' => $this->string()->comment('Type'),
			]);
			
			$this->createTimestamps('files');
		}
		
		public function safeDown()
		{
			$this->dropTable('file_product');
			
			$this->dropTable('files');
			
			return true;
		}
	}
