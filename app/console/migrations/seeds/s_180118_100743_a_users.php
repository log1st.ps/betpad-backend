<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:43
 *
 * Seeds data from users with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100743_a_users extends Seed
{

    public $table = 'users';

    public function columns()
    {
        return [
            'user_id', 'email', 'password', 'authKey', 'parent'
        ];
    }

    public function rows()
    {
        return [
            [ '1','log1st.ps@gmail.com','$2y$13$FYmLcjA/Pd/J0yO21uceIedTQI4KY23TnqdhP6b/pE3wF6K9K.wgK','$2y$13$zoTJQyqHbRwS3rniHVB59u6f14ZBaqw8/MqgBsrUw08Fsal9Bd1Oa','2' ],
        ];
    }
}