<?php
	
	
	use console\components\Migration;
	
	class m170814_030633_seeds extends Migration
	{
		
		public function safeUp()
		{
			$this->createTable('seeds', [
				'seed_id' => $this->primaryKey()->comment('Seed ID'),
				'class' => $this->string(255)->comment('Table name'),
				'apply_time' => $this->integer()->comment('Apply time'),
				'table' => $this->string(255)->comment('Table name'),
			]);
		}
		
		public function safeDown()
		{
			
			
			return true;
		}
	}
