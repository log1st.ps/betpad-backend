<?php
	
	
	use console\components\Migration;
	use console\migrations\seeds\LanguageInitSeed;
	
	class m170823_142129_language_status extends Migration
	{
		public function safeUp()
		{
			$this->addColumn('languages', 'status', $this->boolean()->defaultValue(1)->comment('Status'));
		}
		
		public function safeDown()
		{
			$this->dropColumn('languages', 'status');
			
			return true;
		}
	}
