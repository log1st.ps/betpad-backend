<?php

class m190213_115914_users_name_fields extends \console\components\Migration
{
    public function safeUp()
    {
        $this->addColumn('users', 'firstName', $this->string(1020));
        $this->addColumn('users', 'lastName', $this->string(1020));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'firstName');
        $this->dropColumn('users', 'lastName');

        return true;
    }
}
