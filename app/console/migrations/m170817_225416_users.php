<?php
	
	
	use console\components\Migration;
	
	class m170817_225416_users extends Migration
	{
		/**
		 * Creates all rbac default tables
		 */
		private function runRbacMigration()
		{
			$oldApp = \Yii::$app;
			new \yii\console\Application([
					'id' => 'Command runner',
					'basePath' => '@app',
					'components' => $oldApp->components,
				]
			);
			Yii::$app->runAction('migrate/up', [
				'migrationPath' => '@yii/rbac/migrations/',
				'interactive' => false,
			]);
			Yii::$app = $oldApp;
		}
		
		public function safeUp()
		{
			$this->runRbacMigration();
			
			$this->createTable('users', [
				'user_id' => $this
					->primaryKey()
					->comment('User ID'),
				'email' => $this
					->string(320)
					->comment('Email address'),
				'password' => $this
					->string(255)
					->comment('Password hash'),
			]);
			$this->createTimestamps('users');
			$this->createTimestamps('auth_assignment');
			$this->createTimestamps('auth_item');
			$this->createTimestamps('auth_item_child');
			$this->createTimestamps('auth_rule');
			
			$this->dropForeignKey('auth_assignment_ibfk_1', 'auth_assignment');
			$this->dropPrimaryKey('item_name', 'auth_assignment');
			
			$this->addColumn(
				'auth_assignment',
				'auth_assignment_id',
				$this->primaryKey()->comment('Auth Assignment ID')->first()
			);
			
			$this->alterColumn(
				'auth_assignment',
				'item_name',
				$this->integer()->comment('Auth Item ID')
			);
			
			$this->renameColumn('auth_assignment', 'item_name', 'item_id');
			
			$this->alterColumn('auth_assignment', 'user_id', $this->integer()->comment('User ID'));
			
			$this->addForeignKey(
				'fk-auth_assignment-users',
				'auth_assignment',
				'user_id',
				'users',
				'user_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
			
			$this->dropForeignKey('auth_item_child_ibfk_1', 'auth_item_child');
			$this->dropForeignKey('auth_item_child_ibfk_2', 'auth_item_child');
			
			$this->dropForeignKey('auth_item_ibfk_1', 'auth_item');
			$this->dropPrimaryKey('name', 'auth_item');
			
			$this->addColumn(
				'auth_item',
				'auth_item_id',
				$this->primaryKey()->comment('Auth Item ID')->first()
			);
			
			$this->addForeignKey(
				'fk-auth_assignment-auth_item',
				'auth_assignment',
				'item_id',
				'auth_item',
				'auth_item_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
			
			$this->alterColumn('auth_item_child', 'parent', $this->integer(11)->comment('Auth Item Parent ID'));
			$this->alterColumn('auth_item_child', 'child', $this->integer(11)->comment('Auth Item Child ID'));
			
			$this->addForeignKey(
				'fk-auth_item_child-auth_item_parent',
				'auth_item_child',
				'parent',
				'auth_item',
				'auth_item_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
			
			$this->addForeignKey(
				'fk-auth_item_child-auth_item_child',
				'auth_item_child',
				'child',
				'auth_item',
				'auth_item_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
		}
		
		public function safeDown()
		{
			return false;
		}
	}
