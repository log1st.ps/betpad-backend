<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 13.02.2019
 * Time: 13:33
 */

namespace app\form;


use common\models\User;
use yii\base\Model;

class SignUpForm extends Model
{
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $passwordConfirmation;

    public $tariff;
    public $period;

    public function scenarios()
    {
        return [
            'sign-up' => [
                'firstName',
                'lastName',
                'email',
                'password',
                'passwordConfirmation',
            ]
        ];
    }

    public function rules()
    {
        return [
            ['firstName', 'required'],

            ['lastName', 'required'],

            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['passwordConfirmation', 'required'],
            ['passwordConfirmation', 'compare', 'compareAttribute' => 'password'],
        ];
    }
}