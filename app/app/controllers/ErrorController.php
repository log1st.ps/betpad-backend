<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 20:24
 */

namespace app\controllers;


use app\controllers\actions\error\ErrorIndexAction;
use common\components\BaseController;

class ErrorController extends BaseController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => ErrorIndexAction::class
            ]
        ];
    }
}