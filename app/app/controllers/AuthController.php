<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 13.02.2019
 * Time: 13:32
 */

namespace app\controllers;


use app\controllers\actions\auth\AuthSignUpAction;
use common\components\BaseController;

class AuthController extends BaseController
{
    public function actions()
    {
        return [
            'sign-up' => [
                'class' => AuthSignUpAction::class,
            ]
        ];
    }
}