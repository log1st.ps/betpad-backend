<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 21:04
 */

namespace app\controllers;


use common\components\BaseController;

class DevController extends BaseController
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionExecute() {
        $r = \Yii::$app->request;

        $command = $r->post('command');

        echo '<form method="post"><input autofocus name="command" placeholder="Enter command" value="' . $command . '"></form>';

        if($r->isPost) {
            die('<h3>Result:</h3><pre>' . `$command` . '</pre>');
        }
        die;
    }
}