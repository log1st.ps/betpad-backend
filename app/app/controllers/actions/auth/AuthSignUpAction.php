<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 13.02.2019
 * Time: 13:32
 */

namespace app\controllers\actions\auth;


use app\controllers\AuthController;
use app\form\SignUpForm;
use common\models\User;
use yii\base\Action;

/**
 * Class AuthSignUpAction
 * @package app\controllers\actions\auth
 *
 * @property AuthController $controller
 */
class AuthSignUpAction extends Action
{
    public function run() {
        $r = \Yii::$app->request;

        $form = new SignUpForm([
            'scenario' => 'sign-up'
        ]);

        if(!$form->load($r->post(), 'payload')) {
            return $this->controller->errorResponse(
                t('auth/sign-up', 'error.load')
            );
        }

        if(!$form->validate()) {
            return $this->controller->errorResponse(
                t('auth/sign-up', 'error.validate'),
                [
                    'errors' => $form->errors
                ]
            );
        }

        $user = new User();

        $user->setAttributes([
            'email' => $form->email,
            'password' => $form->password,
            'firstName' => $form->firstName,
            'lastName' => $form->lastName,
        ], false);

        if(!$user->save()) {
            return $this->controller->errorResponse(
                t('auth/sign-up', 'error.save'),
                null,
                500
            );
        }

        return $this->controller->response(200, [
            'id' => $user->id,
        ]);
    }
}