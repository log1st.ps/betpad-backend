<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 20:32
 */

namespace app\controllers\actions\error;

use app\controllers\ErrorController;
use yii\web\HttpException;

/**
 * Class ErrorIndexAction
 * @package app\controllers\actions\error
 *
 * @property ErrorController $controller
 */
class ErrorIndexAction extends \yii\base\Action
{
    public function run() {
        /** @var HttpException $exception */
        $exception = \Yii::$app->errorHandler->exception;

        return $this->controller->response($exception->statusCode, [
            'message' => $exception->getMessage()
        ]);
    }
}