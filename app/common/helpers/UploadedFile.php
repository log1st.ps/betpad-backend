<?php
	/**
	 * Created by PhpStorm.
	 * User: log1st
	 * Date: 06.02.2018
	 * Time: 4:47
	 */
	
	namespace common\helpers;
	
	
	class UploadedFile extends \yii\web\UploadedFile
	{
		
		private static $_files;
		
		public static function files() {
			return self::loadFiles();
		}
		
		/**
		 * @inheritdoc
		 */
		private static function loadFiles()
		{
			if (self::$_files === null) {
				self::$_files = [];
				if (isset($_FILES) && is_array($_FILES)) {
					foreach ($_FILES as $class => $info) {
						self::loadFilesRecursive($class, $info['name'], $info['tmp_name'], $info['type'], $info['size'], $info['error']);
					}
				}
			}
			
			return self::$_files;
		}
		
		/**
		 * @inheritdoc
		 */
		private static function loadFilesRecursive($key, $names, $tempNames, $types, $sizes, $errors)
		{
			if (is_array($names)) {
				foreach ($names as $i => $name) {
					self::loadFilesRecursive($key . '[' . $i . ']', $name, $tempNames[$i], $types[$i], $sizes[$i], $errors[$i]);
				}
			} elseif ((int) $errors !== UPLOAD_ERR_NO_FILE) {
				self::$_files[$key] = [
					'name' => $names,
					'tempName' => $tempNames,
					'type' => $types,
					'size' => $sizes,
					'error' => $errors,
				];
			}
		}
	}