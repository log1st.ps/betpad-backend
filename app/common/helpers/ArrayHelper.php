<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 20:27
	 */
	
	namespace common\helpers;
	
	
	class ArrayHelper extends \yii\helpers\ArrayHelper
	{
		public static function dotExplode($array)
		{
				$newArray = array();
				foreach ($array as $key => $value) {
					$dots = explode(".", $key);
					if (count($dots) > 1) {
						$last = &$newArray[$dots[0]];
						foreach ($dots as $k => $dot) {
							if ($k == 0) continue;
							$last = &$last[$dot];
						}
						$last = $value;
					} else {
						$newArray[$key] = $value;
					}
				}
			return $newArray;
		}
	}