<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 16:13
 */

$config = [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm',
    ],
    'sourceLanguage' => '00',
    'language' => 'en-US',
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => array_merge([
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],
        'request' => [
            'enableCookieValidation' => true,
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => "." . CONST_DOMAIN
            ],
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class,
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
        ],
        'session' => [
            'class' => \common\components\rbac\DbSession::class,
            'sessionTable' => 'sessions',

            'cookieParams' => [
                'lifetime' => 10 * 365 * 24 * 60 * 60,
                'domain' => "." . CONST_DOMAIN,
                'httpOnly' => true,
            ],
        ],
        'i18n' => [
            'class' => \common\components\i18n\I18N::class,
            'messageFormatter' => \common\components\i18n\MessageFormatter::class,
            'translations' => [
                '*' => [
                    'class' => \common\components\i18n\DbMessageSource::class,
                    'db' => 'db',
                    'messageTable' => 'messages',
                    'sourceMessageTable' => 'source_messages',

                    'enableCaching' => YII_CACHE,
                    'cachingDuration' => YII_CACHE_DURATION,
                    'forceTranslation' => !YII_CACHE,

                    'on missingTranslation' => [\common\components\i18n\TranslationEventHandler::class, 'handleMissingTranslation'],
                ],
            ],
        ],
        'authManager' => [
            'class' => \yii\rbac\DbManager::class,
        ],
        'user' => [
            'class' => \common\components\rbac\User::class,
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '@toFrontend/sign-in',
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'domain' => '.' . CONST_DOMAIN,
            ],
        ],
    ], require 'db-local.php'),
    'modules' => [],
    'bootstrap' => [],
];

if(YII_DEBUG) {
    $config['modules']['debug'] = [
        'class' => \yii\debug\Module::class,
        'allowedIPs' => ['*'],
        'traceLine' => '<a href="phpstorm://open?url={file}&line={line}">{file}:{line}</a>',
        'dataPath' => '@runtime/debugbar',
    ];
    $config['components']['log'] = [
        'targets' => [
            [
                'class' => \yii\log\FileTarget::class,
                'levels' => ['error', 'warning'],
            ],
        ],
    ];

    $config['bootstrap'][] = 'debug';
    $config['bootstrap'][] = 'log';
}

return $config;