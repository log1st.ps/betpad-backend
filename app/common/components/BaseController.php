<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 17.01.2019
 * Time: 16:39
 */

namespace common\components;


use common\helpers\ArrayHelper;
use yii\filters\Cors;
use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        if(YII_DEBUG) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            YII_DEBUG ? [
                'corsFilter' => [
                    'class' => Cors::class,
                ]
            ] : []
        );
    }

    public function response($status, $data = null)
    {
        return $this->asJson(ArrayHelper::merge([
            'status' => $status,
        ], $data ? [
            'data' => $data,
        ] : []));
    }

    public function errorResponse(string $message, $data = null, int $status = 400)
    {
        return $this->response($status, ArrayHelper::merge([
            'message' => $message,
        ], $data ? [
            'content' => (array)$data
        ] : []));
    }


}