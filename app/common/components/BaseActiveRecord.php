<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:20
	 */
	
	namespace common\components;
	
	
	use common\components\db\ActiveQuery;
	use Yii;
	use yii\db\ActiveRecord;
	
	/**
	 * Class BaseActiveRecord
	 * @package common\components
	 *
	 * @property integer $id
	 * @property integer $created_at
	 * @property integer $updated_at
	 */
	class BaseActiveRecord extends ActiveRecord
	{
		const STATUS_DISABLED = 0;
		const STATUS_ENABLED = 1;
		
		/**
		 * @return ActiveQuery|object
		 * @throws \yii\base\InvalidConfigException
		 */
		public static function find()
		{
			return Yii::createObject(ActiveQuery::class, [get_called_class()]);
		}
		
		public static function cnc()
		{
			return false;
		}
		
		public static function sortAttribute() {
			return static::idStr();
		}
		
		public static function idStr()
		{
			return 'id';
		}
		
		public static function tableName()
		{
			return parent::tableName();
		}
		
		
		public function attributes()
		{
			if(YII_CACHE) {
				$key = 'attributes-' . $this::shortclass;
				$attributes = Yii::$app->cache->get($key);
				
				if (!$attributes) {
					Yii::$app->cache->set($key, array_keys(static::getTableSchema()->columns));
				}
				
				return Yii::$app->cache->get($key);
			}
			
			return array_keys(static::getTableSchema()->columns);
		}
		
		public function __get($name)
		{
			if ($name === 'id') {
				return $this->{$this::idStr()};
			}
			return parent::__get($name);
		}
		
		public function __toString()
		{
			return (string)$this->id;
		}
		
		public static function shortclass() {
			return basename(str_replace('\\', '/', self::class));
		}
		
		static $all = [];
		
		public static function findAllAsArray($id = null, $caption = null, $condition = [])
		{
			$key = static::class;
			
			if (!isset(static::$all[$key][$id][$caption])) {
				$values = [];
				
				/** @var self $value */
				foreach (static::find()->andWhere($condition)->all() as $value) {
					$values[$value->{$id ?? static::idStr()}] = $caption ? ($caption instanceof \Closure ? call_user_func($caption, $value) : $value->{$caption ?? static::idStr()}) : $value->__toString();
				}
				static::$all[$key][$id][$caption] = $values;
			}
			
			return static::$all[$key][$id][$caption];
		}
		
		protected static $constants = null;
		
		public static function getConstants($prefix)
		{
			if (is_null(static::$constants[static::cnc()] ?? null)) {
				$ref = new \ReflectionClass(self::class);
				
				foreach ($ref->getConstants() as $key => $value) {
					$ex = explode('_', $key);
					$pr = mb_strtolower(array_shift($ex));
					if(!in_array($pr, [ 'op', 'event', 'scenario' ])) {
						$val = array_map(function ($item) {
							return mb_ucfirst($item);
						}, $ex);
						$val[0] = mb_strtolower($val[0]);
						if ($val) {
							static::$constants[static::cnc()][$pr][$value] = t(['model.' . self::shortclass], 'constant.' . $pr . '.' . implode($val));
						}
					}
				}
			}
			
			return static::$constants[static::cnc()][$prefix] ?? null;
		}
		
		public function beforeSave($insert)
		{
			$time = time();
			if($insert) {
				$this->created_at = $time;
			}
			$this->updated_at = $time;
			return parent::beforeSave($insert);
		}
	}