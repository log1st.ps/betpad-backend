<?php
	
	namespace common\components\i18n;
	
	use yii\db\Expression;
	use yii\db\Query;
	use yii\helpers\ArrayHelper;
	use yii\i18n;
	
	class DbMessageSource extends i18n\DbMessageSource
	{
		
		public $messages = [];
		
		protected function loadMessagesFromDb($category, $language)
		{
			$mainQuery = (new Query())->select(
				[
					'message' => 't1.message',
					'translation' => 't2.translation',
					'language' => 'languages.locale',
				])
				->from([
					't1' => $this->sourceMessageTable,
					't2' => $this->messageTable,
				])
				->leftJoin('languages', 'languages.language_id = t2.language')
				->where([
					't1.source_id' => new Expression('[[t2.source]]'),
					't1.category' => $category,
					'languages.locale' => $language,
				]);
			
			$fallbackLanguage = substr($language, 0, 2);
			$fallbackSourceLanguage = substr($this->sourceLanguage, 0, 2);
			
			if ($fallbackLanguage !== $language) {
				$mainQuery->union($this->createFallbackQuery($category, $language, $fallbackLanguage), true);
			} elseif ($language === $fallbackSourceLanguage) {
				$mainQuery->union($this->createFallbackQuery($category, $language, $fallbackSourceLanguage), true);
			}
			
			$messages = $mainQuery->createCommand($this->db);
			
			$messages = $messages->queryAll();
			
			
			$this->messages = $messages;
			$messages = ArrayHelper::map($messages, 'message', 'translation');
			return $messages;
		}
		
		/**
		 * The method builds the [[Query]] object for the fallback language messages search.
		 * Normally is called from [[loadMessagesFromDb]].
		 *
		 * @param string $category         the message category
		 * @param string $language         the originally requested language
		 * @param string $fallbackLanguage the target fallback language
		 * @return Query
		 * @see   loadMessagesFromDb
		 * @since 2.0.7
		 */
		protected function createFallbackQuery($category, $language, $fallbackLanguage)
		{
			return (new Query())->select(['message' => 't1.message', 'translation' => 't2.translation', 'language' => 'languages.locale'])
				->from(['t1' => $this->sourceMessageTable, 't2' => $this->messageTable])
				->where([
					't1.source_id' => new Expression('[[t2.source]]'),
					't1.category' => $category,
					'languages.locale' => $fallbackLanguage,
				])
				->leftJoin('languages', 'languages.language_id = t2.language')
				->andWhere([
					'NOT IN', 't2.source', (new Query())->select('[[source]]')->from($this->messageTable)->where(['languages.locale' => $language])
						->leftJoin('languages', 'languages.language_id = messages.language'),
				]);
		}
	}