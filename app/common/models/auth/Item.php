<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 5:00
	 */
	
	namespace common\models\auth;
	
	
	use common\components\BaseActiveRecord;
	
	/**
	 * Class Item
	 * @package common\models\Auth
	 *
	 * @property integer $auth_item_id
	 * @property string $name
	 * @property integer $type
	 * @property string $description
	 * @property string $rule_name
	 * @property mixed $data
	 * @property integer $created_at
	 * @property integer $updated_at
	 *
	 * @property Assignment[] $_assignments
	 */
	class Item extends BaseActiveRecord
	{
		const TYPE_ROLE = 1;
		const TYPE_PERMISSION = 2;
		
		public static function cnc()
		{
			return 'auth-items';
		}
		
		public static function tableName()
		{
			return 'auth_item';
		}
		
		public static function idStr()
		{
			return 'auth_item_id';
		}
		
		public function __toString()
		{
			return /*self::getTypeCaption($this->type) . ': ' . */
				$this->name;
		}
		
		public function get_assignments()
		{
			return $this->hasMany(Assignment::class, [
				'item_id' => self::idStr(),
			]);
		}
		
		public static function getTypeCaption($type)
		{
			return t(['models::' . self::cnc()], 'type.' . $type);
		}
		
		public static function getTypes()
		{
			return [
				self::TYPE_ROLE => self::getTypeCaption(self::TYPE_ROLE),
				self::TYPE_PERMISSION => self::getTypeCaption(self::TYPE_PERMISSION),
			];
		}
	}