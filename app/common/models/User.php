<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:19
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use common\models\auth\Assignment;
	use yii\base\NotSupportedException;
	use yii\web\IdentityInterface;
	
	
	/**
	 * Class User
	 * @package common\models
	 *
	 * @property integer $user_id
	 * @property string $email
	 * @property string $firstName
	 * @property string $lastName
	 * @property string $password
	 * @property integer $parent
	 *
	 * @property Assignment[] $_assignments
	 *
	 * @property void $authKey
	 */
	class User extends BaseActiveRecord implements IdentityInterface
	{
		public static function cnc()
		{
			return 'users';
		}
		
		public static function tableName()
		{
			return 'users';
		}
		
		public static function sortAttribute()
		{
			return 'name';
		}
		
		public static function idStr()
		{
			return 'user_id';
		}
		
		public function __toString()
		{
			return (string)$this->id;
		}
		
		public function get_assignments()
		{
			return $this->hasMany(Assignment::class, [
				'user_id' => self::idStr(),
			]);
		}
		
		public function get_parent()
		{
			return $this->hasMany(self::class, [
				self::idStr() => 'parent',
			]);
		}
		
		public function get_children()
		{
			return $this->hasMany(self::class, [
				'parent' => self::idStr(),
			]);
		}
		
		// Identity
		public static function findIdentity($id)
		{
			return self::find()->where([
				self::idStr() => $id,
			])->one();
		}
		
		public function getId()
		{
			return $this->id;
		}
		
		public static function findIdentityByAccessToken($token, $type = null)
		{
			return self::find()->where([
				'authkey' => $token
			])->one();
		}
		
		public function getAuthKey()
		{
			return $this->authKey ?? null;
		}
		
		public function validateAuthKey($authKey)
		{
			return $this->authKey === $authKey;
		}
		
		/**
		 * @param $insert
		 * @return bool
		 * @throws \yii\base\Exception
		 */
		public function beforeSave($insert)
		{
			if(empty($this->authKey)) {
				$this->authKey = \Yii::$app->security->generatePasswordHash(uniqid('token'));
			}
			if(empty($this->password)) {
				$this->password = $this->oldAttributes['password'];
			}   else {
				if($insert || ($this->password !== $this->oldAttributes['password'])) {
					$this->password = \Yii::$app->security->generatePasswordHash($this->password);
				}
			}
			return parent::beforeSave($insert);
		}
		
		public function afterFind()
		{
			parent::afterFind();
		}
	}